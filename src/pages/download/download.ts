import 'rxjs/add/operator/switchMap';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IonicPage, Nav, NavController, NavParams } from 'ionic-angular';
import { Headers, RequestOptions } from '@angular/http';
import { HttpService} from '../../services/http.service';

@Component({
    selector: 'page-download',
    templateUrl: 'download.html'
  })
  export class DownloadPage implements OnInit {
    data: any;
    constructor(
      private httpService: HttpService,
      public navCtrl: NavController,
      public navParams: NavParams
      // private router: Router
    ) { }
    productPics: Array<{title: string, modelPic: string}>;
    ngOnInit(): void {
      this.onSubmit();
      this.productPics = [];
    }
    onSubmit() {

      var tokenId = localStorage.getItem('token_array');
      // var tokenId = this.navParams.get('token_array');
      console.log(tokenId);

      return this.httpService.get('productlist.json?compid=1&prodid=1&lang=1').then(
        (success) => {
          if (success) {
            this.productPics.push({ title: success.model_info[tokenId].model_number, modelPic: success.model_info[tokenId].img });
          } else {
            console.log('Error in success');
          }
        })
        .catch(
        (err) => {
          console.log(err);
        }
        )
    }

  }
