import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormControl, Validators, FormBuilder, } from '@angular/forms';
import { AlertController } from 'ionic-angular';
import { HttpService } from './../../services/http.service';



@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})

export class RegisterPage implements OnInit {

  registerForm: FormGroup;
  firstName: FormControl;
  lastName: FormControl;
  emailAddress: FormControl;
  verifyEmail: FormControl;
  address1: FormControl;
  address2: FormControl;
  city: FormControl;
  country: FormControl;
  state: FormControl;
  postalCode: FormControl;
  mobileNumber: FormControl;
  landLineNumber: FormControl;
  choosePrinter: FormControl;
  serialNumber: FormControl;
  purchaseMonth: FormControl;
  purchaseDay: FormControl;
  purchaseYear: FormControl;
  formData : any;
  sucessMessage : any;

  constructor( 
    public navCtrl: NavController, 
    public navParams: NavParams,
    private fb: FormBuilder,
    private httpService: HttpService,
    public alertCtrl: AlertController
  ) {}

  ngOnInit() {
    this.createFormControls();
    this.createForm();
    //console.log(this.onSubmit());
  }
  createFormControls(){
    this.firstName = new FormControl(null, [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+')]);
    this.lastName = new FormControl(null, [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+')]);
    this.emailAddress = new FormControl(null, [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]);
    this.verifyEmail = new FormControl (null, [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]);
    this.address1 = new FormControl (null, Validators.required);
    this.address2 = new FormControl ('');
    this.city = new FormControl(null, [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+')]);
    this.country = new FormControl ('');
    this.state = new FormControl (null, Validators.required);
    this.postalCode = new FormControl (null, [Validators.required, Validators.pattern("[0-6]*"),  Validators.minLength(6)]);
    this.mobileNumber = new FormControl (null, [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$"), Validators.minLength(10)]);
    this.landLineNumber = new FormControl ('');
    this.choosePrinter = new FormControl ('');
    this.serialNumber = new FormControl (null, Validators.required);
    this.purchaseMonth = new FormControl (null, Validators.required);
    this.purchaseDay = new FormControl (null, Validators.required);
    this.purchaseYear = new FormControl (null, Validators.required);
  }

  createForm() {
    this.registerForm = new FormGroup({
      firstName: this.firstName,
      lastName: this.lastName,
      emailAddressValid:this.fb.group({
        emailAddress: this.emailAddress,
        verifyEmail: this.verifyEmail,
      },),
      address1: this.address1,
      address2: this.address2,
      city: this.city,
      country: this.country,
      state: this.state,
      postalCode: this.postalCode,
      mobileNumber: this.mobileNumber,
      landLineNumber: this.landLineNumber,
      choosePrinter: this.choosePrinter,
      serialNumber: this.serialNumber,
      purchaseMonth: this.purchaseMonth,
      purchaseDay: this.purchaseDay,
      purchaseYear: this.purchaseYear,
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  onSubmit(post){    

    if (this.registerForm.valid) {
      this.formData = {
        "first_name": this.firstName,
        "last_name": this.lastName,
        "email": this.emailAddress,
        "verifyEmail": this.verifyEmail,
        "address_first": this.address1,
        "address_second": this.address2,
        "city": this.city,
        "country": this.country,
        "state": this.state,
        "postal_code": this.postalCode,
        "mobile_number": this.mobileNumber,
        "landline_number": this.landLineNumber,
        "choosePrinter": this.choosePrinter,
        "serial_number": this.serialNumber,
        "purchase_month": this.purchaseMonth,
        "purchase_date": this.purchaseDay,
        "purchase_year": this.purchaseYear,
      }
      return this.httpService.post('api/ver1/productregistration', this.formData).then((success) => {
        // console.log(success.data);
        console.log(success);
        if (success.status == 200) {
          this.registerForm.reset();
          this.sucessMessage = "Product Registration successful."
          //console.log(success);
          
        } 
      }).catch(
          (err)=> {
            console.log(err);               
      }) 

    }
  }

  vari($event: any){
    // console.log($event.srcElement.value);
    // document.fForm.emailAddress.value;
    // console.log(($event.target).parent()[0]);
    // console.log($event.target.parentElement);
    // console.log($event.path[0].offsetParent.children[1].childNodes[5][2].value);
    if(this.emailAddress != this.verifyEmail){
      console.log("Email Id mismatch");
    }else{
      console.log("Match Email Id");
    }
  }

  showAlert() {
    let alert = this.alertCtrl.create({
      title: 'New Friend!',
      subTitle: 'Your friend, Obi wan Kenobi, just accepted your friend request!',
      buttons: ['OK']
    });
    alert.present();
  }
}