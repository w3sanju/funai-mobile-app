import 'rxjs/add/operator/switchMap';
import { Component, OnInit, ViewChild } from '@angular/core';
// import {BrowserModule} from '@angular/platform-browser';
// import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { Nav, NavController } from 'ionic-angular';
// import { Component, ViewChild } from '@angular/core';
// import { Nav, NavController } from 'ionic-angular';
// import { HttpClient } from '@angular/common/http';
import{TabsPage} from './../tabs/tabs'

import { CategorySelectionPage } from '../category-selection/category-selection';

import { Headers, RequestOptions } from '@angular/http';
import { HttpService} from '../../services/http.service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

// @Injectable()
export class HomePage implements OnInit{
    data:any;
    constructor(
        private httpService: HttpService,
        public navCtrl: NavController
        // private router: Router
    ) {}

vals : any;

    tabsPage = TabsPage;
    productPics: Array<{title: string, modelPic: string}>;
  @ViewChild(Nav) nav: Nav;

  rootPage: any ;




// anOtherPage: CategorySelectionPage;
goAnOtherPage(tokenNumber) {
// console.log(tokenNumber);
localStorage.clear();
localStorage.setItem('token_array', tokenNumber);
//   this.navCtrl.setRoot(CategorySelectionPage);

this.navCtrl.setRoot(CategorySelectionPage,{'token_array': tokenNumber});

  // this.navCtrl.push(CategorySelectionPage,{'token_array': tokenNumber});
}

private options: RequestOptions;

  // constructor(public platform: Platform, public navCtrl: NavController, private http: HttpClient) {
    // this.productPics = [
    //   { title: 'https://picsum.photos/240/150/'},
    //   { title: 'https://picsum.photos/240/150/'},
    //   { title: 'https://picsum.photos/240/150/'},
    //   { title: 'https://picsum.photos/240/150/'}
    // ];
  // }

  private configure() {
      // let token = localStorage.getItem('token');
      let headers = new Headers();
      // console.log(token);

      headers = new Headers({
          'Content-Type': 'application/json'
      });

      // if(token !== null){
      //     headers = new Headers({
      //         'Content-Type': 'application/json',
      //         'Username': 'funaiadmin',
      //         'Password': 'Fu@dm!n45'
      //     });
      // }else{
      //     headers = new Headers({
      //         'Content-Type': 'application/json',
      //     });
      // }

      this.options = new RequestOptions({ headers: headers });
  }

  ngOnInit(): void {

    this.vals = '';

      // this.myGet();
      // this.myPost();

      // this.myPost(this.vals);

      this.onSubmit();

      this.productPics = [];

    }

  openPage(page) {
    this.nav.setRoot(page.component);
  }

  // private handleError(error: any): Promise<any> {
  //     console.error('An error occurred', error);
  //     return Promise.reject(error.message || error);
  // }

  // myGet()
  // {
  //   this.configure();
  //   this.http.get('http://124.30.44.230/funaihelpver1/api/ver1/productlist.json?compid=1&prodid=1&lang=1').subscribe(padamData => {
  //     console.log(padamData);
  //   },
  //     err => {
  //       console.log("Error occured.");
  //     });
  // }


  // Working Post API

    // myPost()
    // {
    //   this.http.post('https://httpbin.org/post', { })
    //   .subscribe(
    //      (val) => {
    //          console.log("POST call successful value returned in body", val);
    //      },
    //      response => {
    //          console.log("POST call in error", response);
    //      },
    //      () => {
    //          console.log("The POST observable is now completed.");
    //      });
    // }



    // this.http.post('https://httpbin.org/post', {
    //     "Username": "funaiadmin",
    //     "Password": "Fu@dm!n45"
    // })


// http://124.30.44.230/funaihelpver1/api/productregistration

extractData(res: Response) {
  let body = res.json();
  console.log(body);
  return body || {};
}


onSubmit() {
      var data = {
          // Username: 'funaiadmin',
          // Password: 'Fu@dm!n45',
      };

return this.httpService.get('productlist.json?compid=1&prodid=1&lang=1').then(

// return this.httpService.get('countries.json').then(

      // return this.httpService.get('users').then(

      // return this.httpService.get('latest').then(
        (success)=> {
            console.log(success.model_info[1]);
            // if(success.data.isSucceeded == true){
            //   console.log(success.data);
            //
            // }
            if(success){
              // console.log(success);
              // this.productPics.push({title : success.model_info[2].num_id});

              for (var i = 0; i < success.model_info.length; i++) {
                // this.productPics.push({title : success.model_info[2]});
                this.productPics.push({title : success.model_info[i].model_number , modelPic: success.model_info[i].img });
              }

              console.log(this.productPics);

            }else{
                console.log('Error in success');
            }
        })
        .catch(
            //used Arrow function here
            (err)=> {
               console.log(err);
               // this.router.navigate(['/']);
            }
         )
}


}
