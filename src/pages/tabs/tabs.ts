// import { Component } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, NavParams, Tabs} from 'ionic-angular';
// import {ViewChild } from '@angular/core';
import { HomePage} from './../home/home';
import { DownloadPage} from './../download/download';
import { TroubleshootPage} from './../troubleshoot/troubleshoot'
import 'rxjs/add/operator/switchMap';
// import { Headers, RequestOptions } from '@angular/http';
import { HttpService} from '../../services/http.service';

@Component({
    selector: 'page-tabs',
    templateUrl: 'tabs.html'
  })
  export class TabsPage implements OnInit {
    data: any;
    constructor(
      private httpService: HttpService,
      public navCtrl: NavController,
      public navParams: NavParams
      // private router: Router
    ) { }
    // this tells the tabs component which Pages
    // should be each tab's root Page
    activeTab = parseInt(localStorage.getItem('activeTab'));
    homePage = HomePage;
    tab1Root = DownloadPage;
    tab2Root = TroubleshootPage;

    @ViewChild('solutionTabs') tabRef: Tabs;

    ionViewDidEnter() {
      this.tabRef.select(this.activeTab);
      console.log(this.activeTab);
    }


    productPics: Array<{title: string, modelPic: string}>;
    ngOnInit(): void {
      this.onSubmit();
      this.productPics = [];
    }
    onSubmit() {

      var tokenId = localStorage.getItem('token_array');
      // var tokenId = this.navParams.get('token_array');
      console.log(tokenId);

      return this.httpService.get('productlist.json?compid=1&prodid=1&lang=1').then(
        (success) => {
          if (success) {
            this.productPics.push({ title: success.model_info[tokenId].model_number, modelPic: success.model_info[tokenId].img });
          } else {
            console.log('Error in success');
          }
        })
        .catch(
        (err) => {
          console.log(err);
        }
        )
    }


    // constructor(private navCtrl:NavController) {
    //   //console.log(this.activeTab);
    // }

    pushBack() {
      this.navCtrl.pop();
      //console.log('mahesh');
    }
    pushHome() {
      this.navCtrl.push(HomePage);
    }


  }
