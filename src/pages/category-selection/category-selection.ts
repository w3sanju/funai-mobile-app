// import { Component } from '@angular/core';
// import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Component, OnInit, ViewChild } from '@angular/core';
import { IonicPage, Nav, NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import 'rxjs/add/operator/switchMap';
// import { Headers, RequestOptions } from '@angular/http';
import { HttpService} from '../../services/http.service';

@IonicPage()
@Component({
  selector: 'page-category-selection',
  templateUrl: 'category-selection.html',
})
export class CategorySelectionPage implements OnInit {
  data: any;
  constructor(
    private httpService: HttpService,
    public navCtrl: NavController,
    public navParams: NavParams
    // private router: Router
  ) { }

  vals: any;

  tabsPage = TabsPage;

  @ViewChild(Nav) nav: Nav;

  rootPage: any;

  productPics: Array<{ title: string, modelPic: string }>;

  ngOnInit(): void {
    this.vals = '';
    this.onSubmit();
    this.productPics = [];
  }

  onSubmit() {

    // var tokenId = localStorage.getItem('token_array');
    var tokenId = this.navParams.get('token_array');

    return this.httpService.get('productlist.json?compid=1&prodid=1&lang=1').then(
      (success) => {
        if (success) {
          this.productPics.push({ title: success.model_info[tokenId].model_number, modelPic: success.model_info[tokenId].img });
        } else {
          console.log('Error in success');
        }
      })
      .catch(
      (err) => {
        console.log(err);
      }
      )
  }


  goAnOtherPage(tokenNumber) {
    // localStorage.setItem('token_array', tokenNumber);

    this.navCtrl.setRoot(TabsPage);
    this.navCtrl.setRoot(TabsPage,{'token_array': tokenNumber});
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad CategorySelectionPage');
  }

  backButton() {
    this.navCtrl.setRoot(CategorySelectionPage);
  }
  testit() {
    alert('007');
  }

  gotTabPage(tabValue) {
    if (tabValue == 0) {
      localStorage.setItem('activeTab', tabValue);
      this.navCtrl.push(this.tabsPage);
      console.log(tabValue);
    } else {
      localStorage.setItem('activeTab', tabValue);
      this.navCtrl.push(this.tabsPage);
      console.log(tabValue);
    }
  }

}
