import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CategorySelectionPage } from './category-selection';

@NgModule({
  declarations: [
    CategorySelectionPage
  ],
  imports: [
    IonicPageModule.forChild(CategorySelectionPage),
  ],
})
export class CategorySelectionPageModule {}
