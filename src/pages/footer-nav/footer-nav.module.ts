import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FooterNavPage } from './footer-nav';

@NgModule({
  declarations: [
    FooterNavPage,
  ],
  imports: [
    IonicPageModule.forChild(FooterNavPage),
  ],
})
export class FooterNavPageModule {}
