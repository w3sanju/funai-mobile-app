import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpService } from '../services/http.service';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { CategorySelectionPage } from '../pages/category-selection/category-selection';
import { RegisterPage } from '../pages/register/register';


import { FooterNavPage } from '../pages/footer-nav/footer-nav';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TabsPage } from '../pages/tabs/tabs';
import{ DownloadPage } from '../pages/download/download';
import{ TroubleshootPage } from '../pages/troubleshoot/troubleshoot';
import { FooterPage } from '../pages/shared/footer/footer.component';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    FooterNavPage,
    TabsPage,
    DownloadPage,
    TroubleshootPage,
    CategorySelectionPage,
    RegisterPage,
    FooterNavPage,
    FooterPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp,{tabsPlacement: 'top'}),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    TabsPage,
    FooterNavPage,
    DownloadPage,
    TroubleshootPage,
    CategorySelectionPage,
    RegisterPage,
    FooterPage
  ],
  providers: [
    HttpService,
    StatusBar,
    SplashScreen,
    HttpService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
