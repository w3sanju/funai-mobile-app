import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';

import 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class HttpService {

    private apiBaseUrl = "http://124.30.44.230/funaihelpver1/api/ver1";
    // private apiBaseUrl = "http://www.splashbase.co/api/v1/images";

    // private apiBaseUrl = "";
    private options: RequestOptions;

    constructor(private http: Http) {

    }

    private configure() {
        let token = localStorage.getItem('token');
        let headers = new Headers();
        // console.log(token);
        if(token !== null){
            headers = new Headers({
                'Content-Type': 'application/json',
                'Token': token
            });
        }else{
            headers = new Headers({
                'Content-Type': 'application/json',
            });
        }

        this.options = new RequestOptions({ headers: headers });
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

    search(url): Observable<any> {
        url = `${this.apiBaseUrl}/${url}`;

        this.configure();

        return this.http
            .get(url, this.options)
            .map((r: Response) => r.json());
    }


    get(url): Promise<any> {
        url = `${this.apiBaseUrl}/${url}`;

        this.configure();

        return this.http.get(url, this.options)
            .toPromise()
            .then(response => response.json() )
            .catch(this.handleError);
    }

    post(url, data): Promise<any> {
        url = `${this.apiBaseUrl}/${url}`;
        this.configure();

        return this.http
            .post(url, JSON.stringify(data), this.options)
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }


    delete(url): Promise<void> {
        url = `${this.apiBaseUrl}/${url}`;

        this.configure();

        return this.http.delete(url, this.options)
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }
}
